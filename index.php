<?php

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Application;

setlocale(LC_ALL, 'ru_RU.UTF8');

define('BASE_DIR', __DIR__);

require_once BASE_DIR . '/app/config/config.php';
require_once BASE_DIR . '/app/config/loader.php';

// Cоздаём Dependency Injection
$di = new FactoryDefault();

require_once BASE_DIR . '/app/config/services.php';

try {
    $application = new Application($di);
    $response = $application->handle();
    $response->send();
} catch (\Exception $e) {
    echo '<pre>' . nl2br($e->getMessage() . "\n" . $e->getTraceAsString()) . '</pre>';
}
