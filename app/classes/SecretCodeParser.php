<?php

class SecretCodeParser {

  const DIGITS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  const KEYWORD_BEGIN = '{';
  const KEYWORD_END = '}';
  const SIGN = ['+', '-'];

  // Метод, разбирающий строку на секретные коды и возвращающий их в массиве
  public static function parse($text) {
    // Длина строки
    $length = strlen($text);
    // Текущая позиция в строке
    $currentPosition = 0;
    // Позиция начала подстроки
    $startPosition = 0;
    // Результирующий массив секретных кодов
    $result = [];
    // Пока не достигли конца строки - выполняем цикл
    while ($currentPosition < $length) {
      // Если встретился символ, говорящий нам, что это секретный код
      if ($text[$currentPosition] === self::KEYWORD_BEGIN) {
        // Увеличиваем текущую позицию, чтобы проверить следующий символ
        $currentPosition++;
        // Случай, когда секретный код попался со знаком
        if ($currentPosition < $length && self::isSign($text[$currentPosition])) {
          // Запоминаем позицию начала подстроки
          // и увеличиваем позицию на 1,
          // чтобы проверить следующий символ
          $startPosition = $currentPosition++;
          // Если следующий символ начинается с цифры и не достигли конца строки
          if ($currentPosition < $length && self::isDigit($text[$currentPosition])) {
            // Выделяем числовую часть
            self::number($text, $currentPosition);
            // Если следующий символ - закрывающая скобка, значит, получили корректный секретный код
            if ($currentPosition < $length) {
              if ($text[$currentPosition] === self::KEYWORD_END) {
                array_push($result, (int) substr($text, $startPosition, $currentPosition - $startPosition));
              }
            }
          }
        // Случай с секретным кодом без знака
        } else if ($currentPosition < $length && self::isDigit($text[$currentPosition])) {
          $startPosition = $currentPosition++;
          self::number($text, $currentPosition);
          if ($currentPosition < $length) {
            if ($text[$currentPosition] === self::KEYWORD_END) {
              array_push($result, (int) substr($text, $startPosition, $currentPosition - $startPosition));
            }
          }
        }
      }
      $currentPosition++;
    }
    return $result;
  }

  // Приватный метод проверки принадлежности конкретного символа к знаку числа
  private static function isSign($ch) {
    return in_array($ch, self::SIGN);
  }

  // Приватный метод проверки принадлежности конкретного символа к цифре
  private static function isDigit($ch) {
    return in_array($ch, self::DIGITS);
  }

  // Приватный метод выделения подстроки, содержащую число
  private static function number($origin, &$pos) {
    $length = strlen($origin); // Длина строки
    $startPosition = $pos;     // Начальная позиция
    while ($pos < $length && self::isDigit($origin[$pos])) {
      $pos++;
    }
  }

}
