<?php

/**
 * Класс вспомогательных обёрточных функций (утилит)
 *
 * @author Mikhail Matveev
 *
 * @package /app/classes
 */
class Utils {

  /**
   * Возвращает JSON-представление данных
   *
   * @param mixed $data Входные данные
   *
   * @return string Строковое представление в формате JSON
   */
  public static function toJSON($data) {
    return isset($data) && !empty($data) ? json_encode($data) : null;
  }

}
