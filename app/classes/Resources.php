<?php

/**
 * Класс ресурсов
 *
 * @author Mikhail Matveev
 *
 * @package /app/classes
 */
class Resources {
  const CALCULATION_NOT_FOUND = 'Расчет отсутствует';
  const INVALID_ACTION = 'Недопустимое действие';
  const INVALID_OPERATION = 'Ошибка при выполнении запроса!';
  const INVALID_REQUEST_METHOD = 'Неверный метод запроса.';
  const RECORD_ADDED = 'Запись успешно добавлена!';
  const RECORD_UPDATED = 'Запись успешно обновлена!';
  const REQUIRED_FIELD = "Поле \"%s\" является обязательным";
  const STRING_FIELD = "Поле \"%s\" является простой строкой и не должно содержать специальные символы или тэги";
}
