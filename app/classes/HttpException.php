<?php

/**
 * Класс исключения, отвечающий за генерацию ошибки сервера
 *
 * @author Mikhail Matveev
 *
 * @package /app/classes/exceptions
 */
class HttpException extends Exception {

  public function __construct($message = 'Server Error') {
    parent::__construct($message, 500);
  }
}
