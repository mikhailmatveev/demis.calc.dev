<?php

use Phalcon\Mvc\Model;

/**
 * Класс модели вычислений
 *
 * @author Mikhail Matveev
 *
 * @package /app/models
 */
class Calculations extends Model {

  public function getSource() {
    return 'calculations';
  }
}
