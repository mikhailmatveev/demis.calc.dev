<?php

use Phalcon\Mvc\Model;

/**
 * Класс модели вычислений
 *
 * @author Mikhail Matveev
 *
 * @package /app/models
 */
class SecretCodes extends Model {

  public function getSource() {
    return 'secret_codes';
  }
}
