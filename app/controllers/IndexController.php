<?php

/**
 * Основной контроллер
 *
 * @author Mikhail Matveev
 *
 * @package /app/controllers
 */
class IndexController extends BaseController {

  public function indexAction() {
    // Показать только представление, относящееся к конкретному действию контроллера
    $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
  }

}
