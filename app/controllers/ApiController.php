<?php

use Phalcon\Db\Column;
use Phalcon\Http\Response;

/**
 * Основной контроллер
 *
 * @author Mikhail Matveev
 *
 * @package /app/controllers
 */
class ApiController extends BaseController {

  public function addCalcAction() {
    try {
      // Если запрос не был отправлен стандартным методом POST,
      // то генерируем исключение
      if (!$this->request->isPost()) {
        // Некорректный тип запроса
        throw new HttpException(Resources::INVALID_REQUEST_METHOD);
      }
      // Принимаем значения POST-параметров
      $name = $this->request->getPost('name');
      $src  = $this->request->getPost('src');
      // Фильтрация параметров запроса
      if (!isset($name) || empty($name)) {
        throw new HttpException(sprintf(Resources::REQUIRED_FIELD, 'Название расчета'));
      }
      // Создаем новый объект модели расчета
      $calculation = new Calculations();
      $calculation->name = $name;
      $calculation->src = $src;
      // Обновляем id камеры
      $saved = $calculation->save();
      // Если операция завершилась с ошибкой - генерируем исключение
      if (!$saved) {
        throw new HttpException(Resources::INVALID_OPERATION);
      }
      // Массив секретных кодов
      $mySecretCodes = [];
      // Парсим текст расчета, если содержимое не пусто
      // и сохраняем в таблицу секретных кодов значения
      // с привязкой к текущему расчету
      if (!empty($src)) {
      // ID добавленной записи
      $calculationId = $calculation->id;
        // Парсим текст с расчетом
        $mySecretCodes = SecretCodeParser::parse($src);
        // Каждый секретный код из массива пишем в таблицу
        foreach ($mySecretCodes as $code) {
          $secretCode = new SecretCodes();
          $secretCode->calc_id = $calculationId;
          $secretCode->value = $code;
          $secretCode->save();
        }
      }
      // Ответ в случае успеха
      return self::toHttpResponse(Utils::toJSON(['message' => Resources::RECORD_ADDED]));
    } catch (Exception $e) {
      // Ответ при возникновении исключения
      return self::toHttpResponse($e->getMessage(), $e->getCode());
    }
  }

  public function updateCalcAction() {
    try {
      // Если запрос не был отправлен стандартным методом POST,
      // то генерируем исключение
      if (!$this->request->isPost()) {
        // Некорректный тип запроса
        throw new HttpException(Resources::INVALID_REQUEST_METHOD);
      }
      // Принимаем значения POST-параметров
      $id = $this->request->getPost('id');
      $src  = $this->request->getPost('src');
      // Фильтрация параметров запроса
      if (!isset($id) || empty($id)) {
        throw new HttpException(sprintf(Resources::REQUIRED_FIELD, 'ID'));
      }
      // Ищем расчет с заданным ID
      $calculation = Calculations::findFirst($id);
      // Если расчет с таким ID не нашли, выдаем соответствующее исключение
      if (!$calculation) {
        throw new HttpException(Resources::CALCULATION_NOT_FOUND);
      }
      $calculation->src = $src;
      // Обновляем id камеры
      $saved = $calculation->save();
      // Если операция завершилась с ошибкой - генерируем исключение
      if (!$saved) {
        throw new HttpException(Resources::INVALID_OPERATION);
      }
      // Поиск секретных кодов для данного расчета
      $secretCodes = SecretCodes::find([
        'calc_id = :id:',
        'bind' => [
          'id' => (int) $id
        ]
      ]);
      // Если список секретных кодов не пуст, удаляем его и создаем новый
      if (count($secretCodes) > 0) {
        $secretCodes->delete();
      }
      // Массив секретных кодов
      $mySecretCodes = [];
      // Парсим текст расчета, если содержимое не пусто
      // и сохраняем в таблицу секретных кодов значения
      // с привязкой к текущему расчету
      if (!empty($src)) {
        // Парсим текст с расчетом
        $mySecretCodes = SecretCodeParser::parse($src);
        // Каждый секретный код из массива пишем в таблицу
        foreach ($mySecretCodes as $code) {
          $secretCode = new SecretCodes();
          $secretCode->calc_id = (int) $id;
          $secretCode->value = $code;
          $secretCode->save();
        }
      }
      // Ответ в случае успеха
      return self::toHttpResponse(Utils::toJSON(['message' => Resources::RECORD_UPDATED]));
    } catch (Exception $e) {
      // Ответ при возникновении исключения
      return self::toHttpResponse($e->getMessage(), $e->getCode());
    }
  }

  public function getCalcAction() {
    try {
      // Если запрос не был отправлен стандартным методом GET,
      // то генерируем исключение
      if (!$this->request->isGet()) {
        // Некорректный тип запроса
        throw new HttpException(Resources::INVALID_REQUEST_METHOD);
      }
      $id = $this->dispatcher->getParam('id');
      // Если параметр передан в запрос, значит нужно вывести подробную инфу по расчету
      // В противном случае, выводим список отчетов
      if (isset($id) && !empty($id) && $id > 0) {
        // Найти расчет по его ID
        $calculation = Calculations::findFirst($id);
        // Найти секретные коды, привязанные к данному расчету
        $secretCodes = SecretCodes::find([
          'calc_id = :id:',
          'columns' => 'value',
          'bind' => [
            'id' => $id
          ]
        ]);
        $calculationDetails = [
          'calculation' => $calculation->toArray(),
          'secretCodes' => $secretCodes->toArray()
        ];
        return self::toHttpResponseJson($calculationDetails);
      } else {
        // Критерий поиска (больше, меньше или равно)
        $cond = $this->request->getQuery('cond');
        // Значение, по которому производится поиск
        $q = $this->request->getQuery('q');
        // Если параметр q задан и имеет числовое значение
        if (isset($q) && is_numeric($q)) {
          $queryMap = [
            'gt' => 'SELECT DISTINCT Calculations.id, Calculations.name FROM Calculations, SecretCodes WHERE SecretCodes.calc_id = Calculations.id AND SecretCodes.value > :val:',
            'lt' => 'SELECT DISTINCT Calculations.id, Calculations.name FROM Calculations, SecretCodes WHERE SecretCodes.calc_id = Calculations.id AND SecretCodes.value < :val:',
            'eq' => 'SELECT DISTINCT Calculations.id, Calculations.name FROM Calculations, SecretCodes WHERE SecretCodes.calc_id = Calculations.id AND SecretCodes.value = :val:'
          ];
          // Начало запроса
          $queryCond = $queryMap['eq'];
          // Меняем запрос на соответствующий, если подходящий оператор сравнения имеется в массиве
          if (array_key_exists($cond, $queryMap)) {
            $queryCond = $queryMap[$cond];
          }
          // Окончание запроса
          $queryOrder = 'ORDER BY Calculations.id';
          // Запрос целиком
          $query = "{$queryCond} {$queryOrder}";
          // Получить список расчетов по заданному условию
          $calculationList = $this->modelsManager->executeQuery($query, [
            'val' => (int) $q
          ], [
            'val' => Column::BIND_PARAM_INT
          ]);
          return self::toHttpResponseJson($calculationList->toArray());
        } else {
          // Получить список расчетов
          $calculationList = Calculations::find([
            'columns' => 'id, name',
            'order' => 'id'
          ]);
          return self::toHttpResponseJson($calculationList->toArray());
        }
      }
    } catch (Exception $e) {
      // Ответ при возникновении исключения
      return self::toHttpResponse($e->getMessage(), 200);
    }
  }

  /**
   * Возвращает по-умолчанию 200 ответ (обертка)
   *
   * @param mixed $content Контент
   * @param int $statusCode Код ответа сервера
   *
   * @return Phalcon\Http\Response
   */
  protected static function toHttpResponse($content, $statusCode = 200) {
    return (new Response())->setStatusCode($statusCode)->setContent($content);
  }

  /**
   * Возвращает по-умолчанию 200 ответ в формате JSON (обертка)
   *
   * @param mixed $content Контент
   * @param int $statusCode Код ответа сервера
   *
   * @return Phalcon\Http\Response
   */
  protected static function toHttpResponseJson($content, $statusCode = 200) {
    return (new Response())->setStatusCode($statusCode)->setJsonContent($content)->setContentType('application/json');
  }

}
