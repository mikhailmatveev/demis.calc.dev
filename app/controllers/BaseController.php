<?php

use Phalcon\Mvc\Controller;

/**
 * Класс базового контроллера
 *
 * Предоставляет свойства и методы, которые могут быть использованы
 * в дочерних контроллерах.
 *
 * @author Mikhail Matveev
 *
 * @package /app/controllers
 */
class BaseController extends Controller {

  public function initialize() {}

}
