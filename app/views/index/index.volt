{{ partial('partials/header') }}
<main role="main">
  <div class="container-fluid">
    <h1 class="text-center">Секретные расчеты</h1>
    <div class="input-group-wrapper">
      <div class="input-group">
        <input type="text" placeholder="Найти" class="form-control" id="search-calc-input">
        <div class="input-group-append">
          <button type="button" class="btn btn-outline-secondary" id="search-calc-btn">Поиск</button>
        </div>
      </div>
      <select name="search-op-select" class="form-control" id="search-op-select">
        <option value="gt">Больше</option>
        <option value="lt">Меньше</option>
        <option value="eq">Равно</option>
      </select>
    </div>
    <div class="row">
      <div class="col">
        <h3 class="text-center">Список расчетов</h3>
        <div class="list-group" id="calc-list-group"></div>
      </div>
      <div class="col">
        <h3 class="text-center">Текст расчета</h3>
        <textarea name="calc-textarea" value="" cols="30" rows="10" class="form-control" id="calc-textarea"></textarea>
      </div>
      <div class="col">
        <h3 class="text-center">Список секретных кодов</h3>
        <ul class="list-group" id="secret-codes-list-group"></ul>
      </div>
    </div>
    <div class="button-wrapper">
      <button type="button" data-toggle="modal" data-target="#new-calc-modal" class="btn btn-outline-dark" id="add-calc-btn">Новый расчет</button>
      <button type="button" class="btn btn-outline-dark" id="update-calc-btn">Обновить текущий расчет</button>
    </div>
  </div>
</main>
<div class="modal fade" id="new-calc-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Создать новый расчет</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form novalidate>
          <div class="form-group">
            <label for="calc-name-input-modal">Название расчета</label>
            <input type="text" name="calc-name-input-modal" class="form-control" id="calc-name-input-modal">
          </div>
          <div class="form-group">
            <label for="calc-src-textarea-modal">Текст расчета</label>
            <textarea name="calc-src-textarea-modal" rows="10" class="form-control" id="calc-src-textarea-modal"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="create-calc-btn-modal">Создать</button>
      </div>
    </div>
  </div>
</div>
{{ partial('partials/footer') }}
