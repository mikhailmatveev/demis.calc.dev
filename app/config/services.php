<?php

use Exception;
use Phalcon\Assets\Manager as AssetsManager;
use Phalcon\Db\Adapter\Pdo\Postgresql as DbAdapter;
use Phalcon\Events\Event;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\Dispatcher as MvcDispatcher;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Dispatcher\Exception as DispatchException;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;

// Настраиваем сервис для работы с БД
$di->set('db', function () use ($config) {
  return new DbAdapter([
    'host'     => $config['db']['host'],
    'username' => $config['db']['username'],
    'password' => $config['db']['password'],
    'dbname'   => $config['db']['name']
  ]);
});

$di->set('dispatcher', function () {
  // Создаем менеджер событий
  $eventsManager = new EventsManager();
  // Прикрепляем слушателя
  $eventsManager->attach('dispatch:beforeException', function (Event $event, $dispatcher, Exception $exception) {
    // Handle 404 exceptions
    if ($exception instanceof DispatchException) {
      $dispatcher->forward([
        'controller' => 'index',
        'action'     => 'notfound',
      ]);
      return false;
    }
    // Alternative way, controller or action doesn't exist
    switch ($exception->getCode()) {
      case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
      case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
        $dispatcher->forward([
          'controller' => 'index',
          'action'     => 'notfound',
        ]);
        return false;
      }
  });
  $dispatcher = new MvcDispatcher();
  // Связываем менеджер событий с диспетчером
  $dispatcher->setEventsManager($eventsManager);
  return $dispatcher;
});

// Models Manager
$di->set('modelsManager', function() {
  return new ModelsManager();
});

$di->set('router', function () {
  return require BASE_DIR . '/app/config/routes.php';
});

$di->set('url', function() use ($config) {
  $url = new UrlProvider();
  $url->setBaseUri($config['baseUri']);
  $url->setStaticBaseUri($config['staticBaseUri']);
  return $url;
}, true);

$di->set('view', function() use ($config, $di) {
  $view = new View();
  $view->setViewsDir($config['viewsDir']);
  $view->registerEngines([
    '.volt' => function($view, $di) use ($config) {
      $volt = new VoltEngine($view, $di);
      $volt->setOptions([
        'compiledPath' => $config['cacheDir'] . '/volt',
        'compiledSeparator' => '_'
      ]);
      $compiler = $volt->getCompiler();
      $compiler->addFunction('strtotime', 'strtotime');
      return $volt;
    }
  ]);
  return $view;
}, true);
