<?php

$loader = new \Phalcon\Loader();

$loader->registerDirs([
  $config['cacheDir'],
  $config['classesDir'],
  $config['controllersDir'],
  $config['formsDir'],
  $config['libraryDir'],
  $config['modelsDir'],
  $config['pluginsDir'],
  $config['viewsDir']
]);

// Регистрация сторонних классов
$loader->registerClasses([
  'HttpException.php' => $config['classesDir'] . '/HttpException.php',
  'Resources'         => $config['classesDir'] . '/Resources.php',
  'SecretCodeParser'  => $config['classesDir'] . '/SecretCodeParser.php',
  'Utils'             => $config['classesDir'] . '/Utils.php'
]);

$loader->register();
