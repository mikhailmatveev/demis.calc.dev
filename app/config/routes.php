<?php

// Создание маршрутизатора
$router = new \Phalcon\Mvc\Router();

$router->add('/', 'index::index');
$router->add('/api/calculations', 'api::getcalc');
$router->add('/api/calculations/{id:\d+}', 'api::getcalc');
$router->addPost('/api/calculations/add', 'api::addcalc');
$router->addPost('/api/calculations/update', 'api::updatecalc');

$router->removeExtraSlashes(true);

return $router;
