<?php

$config = [
  'baseUri'        => '/',
  'staticBaseUri'  => '/public',
  'cacheDir'       => BASE_DIR . '/app/cache',
  'classesDir'     => BASE_DIR . '/app/classes',
  'controllersDir' => BASE_DIR . '/app/controllers',
  'formsDir'       => BASE_DIR . '/app/forms',
  'libraryDir'     => BASE_DIR . '/app/library',
  'modelsDir'      => BASE_DIR . '/app/models',
  'pluginsDir'     => BASE_DIR . '/app/plugins',
  'viewsDir'       => BASE_DIR . '/app/views',
  'db' => [
    'host'     => 'localhost',
    'username' => 'mikhail',
    'password' => 'myp@ssw0rd',
    'name'     => 'demis_calculations',
    'schema'   => 'public'
  ]
];
