$().ready(() => {

  var $calcListGroup = $('#calc-list-group'),
    $calcTextarea = $('#calc-textarea'),
    $secretCodesListGroup = $('#secret-codes-list-group'),
    $calcNameInputModal = $('#calc-name-input-modal'),
    $updateCalcBtn = $('#update-calc-btn'),
    $searchCalcInput = $('#search-calc-input'),
    $searchCalcBtn = $('#search-calc-btn'),
    $searchOpSelect = $('#search-op-select'),
    $calcSrcTextareaModal = $('#calc-src-textarea-modal'),
    $createCalcBtnModal = $('#create-calc-btn-modal');

  var currentCalcultionId = -1;

  function loadCalculationList() {
    return $.ajax({
      url: '/api/calculations/',
      method: 'GET',
      dataType: 'json'
    });
  }

  function loadCalculationDetails(id) {
    return $.ajax({
      url: `/api/calculations/${id}`,
      method: 'GET',
      dataType: 'json'
    });
  }

  function clearCalculationList() {
    $calcListGroup.empty();
  }

  function clearCalculationDetails() {
    $calcTextarea.val('');
    $secretCodesListGroup.empty();
  }

  function renderCalculationList(data) {
    if ($.isArray(data) && data.length > 0) {
      data.forEach(item => {
        $calcListGroup.append('<a href="#" data-id="' + item.id + '" class="list-group-item list-group-item-action">' + item.name + '</a>');
      });
    }
  }

  function renderCalculationDetails(data) {
    if ($.isPlainObject(data)) {
      $calcTextarea.val(data.calculation.src);
      if ($.isArray(data.secretCodes) && data.secretCodes.length > 0) {
        data.secretCodes.forEach(item => {
          $secretCodesListGroup.append('<li class="list-group-item">' + item.value + '</li>');
        });
      }
    }
  }

  function updateCalculationList(data) {
    // Очистить список секретных кодов
    clearCalculationList();
    // Отобразить заново новый список
    renderCalculationList(data);
  }

  function updateCalculationDetails(data) {
    // Очистить список секретных кодов
    $secretCodesListGroup.empty();
    // Отобразить заново новый список
    renderCalculationDetails(data);
  }

  // Отобразить список расчетов при первоначальной загрузке страницы
  loadCalculationList().done(renderCalculationList);

  // Создание нового расчета
  $createCalcBtnModal.on('click', function(e) {
    var $this = $(this);
    // Запрет действия по умолчанию
    e.preventDefault();
    // На время запроса делаем кнопку недоступной
    $this.attr('disabled', '');
    // Запрос на сервер
    $.ajax({
      url: '/api/calculations/add',
      method: 'POST',
      dataType: 'json',
      data: $.param({
        name: $calcNameInputModal.val(),
        src: $calcSrcTextareaModal.val()
      })
    }).done(result => {
      // alert(result.message);
      // Запрос на получение списка расчетов
      loadCalculationList().done(updateCalculationList);
      // Очистить поля ввода
      $calcNameInputModal.val('');
      $calcSrcTextareaModal.val('');
    }).fail(err => {
      alert(err.responseText);
    }).always(() => {
      $this.removeAttr('disabled');
    });
  });

  // Обновить текущий расчет
  $updateCalcBtn.on('click', function(e) {
    var $this = $(this);
    // Запрет действия по умолчанию
    e.preventDefault();
    // На время запроса делаем кнопку недоступной
    $this.attr('disabled', '');
    // Запрос на сервер
    $.ajax({
      url: '/api/calculations/update',
      method: 'POST',
      dataType: 'json',
      data: $.param({
        id: currentCalcultionId,
        src: $calcTextarea.val()
      })
    }).done(result => {
      // alert(result.message);
      // Запрос на получение конкретного расчета
      loadCalculationDetails(currentCalcultionId).done(updateCalculationDetails);
    }).fail(err => {
      alert(err.responseText);
    }).always(() => {
      $this.removeAttr('disabled');
    });
  });

  $searchCalcBtn.on('click', function(e) {
    var $this = $(this);
    // Запрет действия по умолчанию
    e.preventDefault();
    // На время запроса делаем кнопку недоступной
    $this.attr('disabled', '');
    // Запрос на сервер
    $.ajax({
      url: '/api/calculations',
      method: 'GET',
      dataType: 'json',
      data: $.param({
        q: $searchCalcInput.val(),
        cond: $searchOpSelect.val()
      })
    }).done(result => {
      // alert(result.message);
      // Запрос на получение списка расчетов
      updateCalculationList(result);
      // Очистить поля ввода
      clearCalculationDetails();
      // Сбросить ID текущего расчета
      currentCalcultionId = -1;
    }).fail(err => {
      alert(err.responseText);
    }).always(() => {
      $this.removeAttr('disabled');
    });
  });

  $(document).on('click', '#calc-list-group a', function(e) {
    var $this = $(this),
      id;
    // Запрет действия по умолчанию
    e.preventDefault();
    // ID текущего расчета
    id = $this.data('id');
    // Запрос на получение конкретного расчета
    loadCalculationDetails(id).done(function(data) {
      // Вывести обновленную информацию о расчете
      updateCalculationDetails(data);
      // Сохрнаить ID текущего расчета в переменную
      currentCalcultionId = id;
    });
  });

});
